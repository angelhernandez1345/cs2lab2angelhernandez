package edu.westga.cs1302.music.view;

import java.io.File;
import java.io.FileNotFoundException;

import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.resources.UI;
import edu.westga.cs1302.music.viewmodel.MusicViewModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

/**
 * The controller behind the GUI file MusicGui.fxml
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class MusicGuiCodeBehind {

	private MusicViewModel theViewModel;
	private ObjectProperty<Song> selectedSong;

	@FXML
	private Pane pane;

	@FXML
	private MenuItem fileOpenMenuItem;

	@FXML
	private MenuItem fileSaveMenuItem;

	@FXML
	private TextField titleTextField;

	@FXML
	private TextField artistTextField;

	@FXML
	private TextField minutesTextField;

	@FXML
	private TextField secondsTextField;

	@FXML
	private TextField yearTextField;

	@FXML
	private Label yearInvalidLabel;

	@FXML
	private Label minutesInvalidLabel;

	@FXML
	private Label secondsInvalidLabel;

	@FXML
	private Button addButton;

	@FXML
	private Button updateButton;

	@FXML
	private ListView<Song> songsListView;

	@FXML
	private Button deleteButton;

	@FXML
	private TextArea summaryTextArea;

	/**
	 * Instantiates a new gui code behind.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MusicGuiCodeBehind() {
		this.theViewModel = new MusicViewModel();
		this.selectedSong = new SimpleObjectProperty<Song>();
	}

	/**
	 * Initializes the GUI components, binding them to the view properties
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.setupListenerForListView();
	}

	private void setupListenerForListView() {
		this.songsListView.getSelectionModel().selectedItemProperty().addListener((observable, oldAuto, newSong) -> {
			if (newSong != null) {
				this.titleTextField.textProperty().set(newSong.getTitle());
				this.artistTextField.textProperty().set(newSong.getArtist());
				Integer minutes = newSong.getMinutes();
				this.minutesTextField.textProperty().set(minutes.toString());
				Integer seconds = newSong.getSeconds();
				this.secondsTextField.textProperty().set(seconds.toString());
				Integer year = newSong.getYear();
				this.yearTextField.textProperty().set(year.toString());
				this.selectedSong.set(newSong);
			}
		});
	}

	private void bindComponentsToViewModel() {
		this.titleTextField.textProperty().bindBidirectional(this.theViewModel.titleProperty());
		this.artistTextField.textProperty().bindBidirectional(this.theViewModel.artistProperty());
		this.yearTextField.textProperty().bindBidirectional(this.theViewModel.yearProperty());
		this.minutesTextField.textProperty().bindBidirectional(this.theViewModel.minutesProperty());
		this.secondsTextField.textProperty().bindBidirectional(this.theViewModel.secondsProperty());
		this.selectedSong.bindBidirectional(this.theViewModel.selectedSongProperty());

		this.songsListView.itemsProperty().bind(this.theViewModel.songsProperty());
		this.summaryTextArea.textProperty().bind(this.theViewModel.playlistSummaryProperty());
	}

	private void showAlert(String title, String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		Window owner = this.pane.getScene().getWindow();
		alert.initOwner(owner);
		alert.setTitle(title);
		alert.setContentText(message);
		alert.showAndWait();
	}

	@FXML
	private void onAddSong() {
		try {
			this.theViewModel.addSong();
		} catch (IllegalArgumentException exception) {
			this.showAlert(UI.Text.ALERT_TITLE_ADD_SONG, exception.getMessage());
		}
	}

	@FXML
	private void onUpdateSong() {
		try {
			this.theViewModel.updateSong();
		} catch (IllegalArgumentException exception) {
			this.showAlert(UI.Text.ALERT_TITLE_UPDATE_SONG, exception.getMessage());
		}
	}

	@FXML
	private void onDeleteSong() {
		this.theViewModel.deleteSong();
	}

	@FXML
	private void onFileOpen() {
		try {
			FileChooser fileChooser = new FileChooser();
			this.setFileFilters(fileChooser);
	
			Window owner = this.pane.getScene().getWindow();
			File selectedFile = fileChooser.showOpenDialog(owner);
	
			if (selectedFile != null) {
				this.theViewModel.loadPlaylist(selectedFile);
				
			} 
		} catch (FileNotFoundException exception) {
			this.showAlert(UI.Text.ALERT_TITLE_FILE_OPEN, exception.getMessage());
		}
	}

	@FXML
	private void onFileSave() {
		try {
			FileChooser fileChooser = new FileChooser();
			this.setFileFilters(fileChooser);
	
			Window owner = this.pane.getScene().getWindow();
			File selectedFile = fileChooser.showSaveDialog(owner);
	
			if (selectedFile != null) {
				this.theViewModel.savePlaylist(selectedFile);
			}
		} catch (FileNotFoundException exception) {
			this.showAlert(UI.Text.ALERT_TITLE_FILE_SAVE, exception.getMessage());
		}
	}

	private void setFileFilters(FileChooser fileChooser) {
		ExtensionFilter filter = new ExtensionFilter("Music Playlist", "*.mpl");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + "/Desktop"));
		fileChooser.getExtensionFilters().add(filter);
		filter = new ExtensionFilter("All Files", "*.*");
		fileChooser.getExtensionFilters().add(filter);
	}

}
