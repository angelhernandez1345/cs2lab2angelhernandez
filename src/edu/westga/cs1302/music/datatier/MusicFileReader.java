package edu.westga.cs1302.music.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.resources.UI;

/**
 * Reads an .mpl (music-playlist) file which is a CSV file with the following
 * format: title,artist,minutes,seconds,year
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class MusicFileReader {

	public static final String FIELD_SEPARATOR = ",";

	@SuppressWarnings("unused")
	private File mplFile;

	/**
	 * Instantiates a new file reader.
	 *
	 * @precondition mplFile != null
	 * @postcondition none
	 * @param mplFile the music playlist file
	 */
	public MusicFileReader(File mplFile) {
		if (mplFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_FILE_NULL);

		}

		this.mplFile = mplFile;
	}

	/**
	 * Opens the associated mpl file and reads all the songs in the file one line at
	 * a time. Parses each line and creates a song object and stores it in an
	 * ArrayList of Song objects. Once the file has been completely read, the
	 * ArrayList of Song objects is returned from the method. Assumes all songs in
	 * the file belong to the same playlist.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return Collection of Song objects read from the file.
	 * @throws FileNotFoundException
	 */
	public ArrayList<Song> loadAllSongs() throws FileNotFoundException {

		ArrayList<Song> songs = new ArrayList<Song>();
		
		try (Scanner input = new Scanner(this.mplFile)) {
			input.useDelimiter(",|\r\n|\n");
			while (input.hasNext()) {
				
				String title = input.next();
				String artist = input.next();
				int minute = input.nextInt();
				int seconds = input.nextInt();
				int year = input.nextInt();
				
				Song song = new Song(title, artist, minute, seconds, year);
				songs.add(song);
			} 
		}
		return songs;
	}
}
