package edu.westga.cs1302.music.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.resources.UI;

/**
 * Writes an .mpl (music-playlist) file which is a CSV file with the following
 * format: title,artist,minutes,seconds,year
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class MusicFileWriter {

	@SuppressWarnings("unused")
	private File mplFile;

	/**
	 * Instantiates a new music file writer.
	 *
	 * @precondition mplFile != null
	 * @postcondition none
	 * 
	 * @param mplFile the music playlist file
	 */
	public MusicFileWriter(File mplFile) {
		if (mplFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_FILE_NULL);
		}

		this.mplFile = mplFile;
	}

	/**
	 * Writes all the songs in the playlist to the specified mpl file. Each song
	 * will be on a separate line and of the following format:
	 * title,artist,minutes,seconds,year
	 * 
	 * @precondition songs != null
	 * @postcondition none
	 * 
	 * @param songs the collection of songs to write to file.
	 * @throws FileNotFoundException
	 */
	public void write(ArrayList<Song> songs) throws FileNotFoundException {
		 if (songs == null) {
			 throw new IllegalArgumentException(UI.ExceptionMessages.SONGS_NULL);
		 }
			PrintWriter writer = new PrintWriter(this.mplFile);
			for (Song currSong : songs) {
				String title = currSong.getTitle();
				String artist = currSong.getArtist();
				int minutes = currSong.getMinutes();
				int seconds = currSong.getSeconds();
				int year = currSong.getYear();
				
				writer.print(title);
				writer.print(",");
				writer.print(artist);
				writer.print(",");
				writer.print(minutes);
				writer.print(",");
				writer.print(seconds);
				writer.print(",");
				writer.println(year);
			
			}
			writer.close(); 
	}
}
