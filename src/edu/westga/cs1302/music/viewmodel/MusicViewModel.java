package edu.westga.cs1302.music.viewmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import edu.westga.cs1302.music.datatier.MusicFileReader;
import edu.westga.cs1302.music.datatier.MusicFileWriter;
import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.output.ReportGenerator;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The class MusicViewModel.
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class MusicViewModel {

	private Playlist playlist;
	private ReportGenerator report;

	private StringProperty titleProperty;
	private StringProperty artistProperty;
	private StringProperty minutesProperty;
	private StringProperty secondsProperty;
	private StringProperty yearProperty;
	private ObjectProperty<Song> selectedSongProperty;

	private ListProperty<Song> songsProperty;
	private StringProperty playlistSummaryProperty;

	/**
	 * Instantiates a music view model.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MusicViewModel() {

		this.playlist = new Playlist("Angel's");
		this.report = new ReportGenerator();

		this.titleProperty = new SimpleStringProperty();
		this.artistProperty = new SimpleStringProperty();
		this.minutesProperty = new SimpleStringProperty();
		this.secondsProperty = new SimpleStringProperty();
		this.yearProperty = new SimpleStringProperty();
		this.selectedSongProperty = new SimpleObjectProperty<Song>();

		this.songsProperty = new SimpleListProperty<Song>(FXCollections.observableArrayList(this.playlist.getSongs()));
		this.playlistSummaryProperty = new SimpleStringProperty();
	}

	/**
	 * The title property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the title property
	 */
	public StringProperty titleProperty() {
		return this.titleProperty;
	}

	/**
	 * The artist property.
	 * 
	 * @precondition
	 * @postcondition
	 * @return the artist property
	 */
	public StringProperty artistProperty() {
		return this.artistProperty;
	}

	/**
	 * The year property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year property
	 */
	public StringProperty yearProperty() {
		return this.yearProperty;
	}

	/**
	 * The minutes property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the minutes property
	 */
	public StringProperty minutesProperty() {
		return this.minutesProperty;
	}

	/**
	 * The seconds property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the seconds property
	 */
	public StringProperty secondsProperty() {
		return this.secondsProperty;
	}

	/**
	 * The selected song property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected song property
	 */
	public ObjectProperty<Song> selectedSongProperty() {
		return this.selectedSongProperty;
	}

	/**
	 * The songs property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of songs property
	 */
	public ListProperty<Song> songsProperty() {
		return this.songsProperty;
	}

	/**
	 * Playlist summary property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the playlist summary property
	 */
	public StringProperty playlistSummaryProperty() {
		return this.playlistSummaryProperty;
	}

	/**
	 * Update selected song
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if successful
	 */
	public boolean updateSong() {
		Song song = this.selectedSongProperty.get();
		if (song != null) {
			String title = this.titleProperty.get();
			String artist = this.artistProperty.get();
			String minutes = this.minutesProperty.get();
			String seconds = this.secondsProperty.get();
			String year = this.yearProperty.get();

			song.setTitle(title);
			song.setArtist(artist);
			song.setMinutes(Integer.valueOf(minutes));
			song.setSeconds(Integer.valueOf(seconds));
			song.setYear(Integer.valueOf(year));

			this.resetProperties();
			return true;
		}

		return false;
	}

	private void resetProperties() {
		this.titleProperty.set("");
		this.artistProperty.set("");
		this.minutesProperty.set("");
		this.secondsProperty.set("");
		this.yearProperty.set("");
		String summaryReport = this.report.buildFullSummaryReport(this.playlist);
		this.playlistSummaryProperty.set(summaryReport);
		this.songsProperty.set(FXCollections.observableArrayList(this.playlist.getSongs()));

	}

	/**
	 * Adds a song with specified attributes to the playlist.
	 *
	 * @return true if song added successfully, false otherwise
	 */
	public boolean addSong() {
		String name = this.titleProperty.get();
		String artist = this.artistProperty.get();
		int minutes = Integer.valueOf(this.minutesProperty.get());
		int seconds = Integer.valueOf(this.secondsProperty.get());
		int year = Integer.valueOf(this.yearProperty.get());

		Song song = new Song(name, artist, minutes, seconds, year);
		if (this.playlist.add(song)) {
			this.resetProperties();
			return true;
		}
		return false;
	}

	/**
	 * Delete selected song from the playlist.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if song successfully deleted, false otherwise
	 */
	public boolean deleteSong() {
		 Song song = this.selectedSongProperty.get();
		 if (this.playlist.removeSong(song)) {
			 this.resetProperties();
			 return true;
		 }
		 
		return false;
	}

	/**
	 * Save the playlist to the specified File.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param file the file to save the playlist to
	 * @throws FileNotFoundException
	 */
	public void savePlaylist(File file) throws FileNotFoundException {
		MusicFileWriter writer = new MusicFileWriter(file);
		writer.write(this.playlist.getSongs());
	}

	/**
	 * Loads the playlist file using the specified File object.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param file The file to load the playlist from
	 * @throws FileNotFoundException
	 */
	public void loadPlaylist(File file) throws FileNotFoundException {
		MusicFileReader reader = new MusicFileReader(file);
		ArrayList<Song> songs = new ArrayList<Song>();
		songs = reader.loadAllSongs();
		
		for (Song song : songs) {
			this.playlist.add(song);
		}
		this.resetProperties();
			
	}
}
