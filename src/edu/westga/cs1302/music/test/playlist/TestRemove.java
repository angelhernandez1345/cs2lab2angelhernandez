package edu.westga.cs1302.music.test.playlist;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.model.Song;

/**
 * Ensures correct functionality of the remove method.
 * 
 * @author Angel Hernandez
 * @version 8/27/2021
 */
public class TestRemove {

	@Test
	public void testRemoveWithOneSongInPlaylist() {
		Song song = new Song("Title1", "Artist1", 1, 0, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song);
		mySongs.removeSong(song);
		assertEquals(0, mySongs.size());
	}

	@Test
	public void testRemoveWithMultipleSongsInPlaylist() {
		Song song1 = new Song("Title1", "Artist1", 1, 0, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 0, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 0, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		mySongs.removeSong(song2);
	
		assertEquals(2, mySongs.size());
		assertEquals(song1, mySongs.getSongs().get(0));
		assertEquals(song3, mySongs.getSongs().get(1));
	}

}
