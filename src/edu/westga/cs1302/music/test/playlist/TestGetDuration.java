package edu.westga.cs1302.music.test.playlist;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.model.Song;

/**
 * Ensures correct functionality of the getDuration method.
 * @author Angel Hernandez
 * @version 8/30/2021
 *
 */
public class TestGetDuration {

	@Test
	public void testAddMultipleSongs() {
		Song song1 = new Song("Title1", "Artist1", 1, 12, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 13, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 50, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		
		int result = mySongs.getDuration();
		
		assertEquals(4, result);
	}

}
