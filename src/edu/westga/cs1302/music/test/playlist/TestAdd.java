package edu.westga.cs1302.music.test.playlist;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.resources.UI;

/**
 * Ensures correct functionality of the add method.
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class TestAdd {

	@Test
	public void testAddNullSong() {
		Playlist mySongs = new Playlist("My playlist");
		IllegalArgumentException iae = assertThrows(IllegalArgumentException.class, () -> mySongs.add(null));
		assertEquals(UI.ExceptionMessages.SONG_NULL, iae.getMessage());
		assertEquals(0, mySongs.size());
	}

	@Test
	public void testAddSongToEmptyPlaylist() {
		Song song = new Song("Title1", "Artist1", 1, 0, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song);
		assertAll(()-> assertEquals(1, mySongs.size()),
				  ()-> assertEquals(song, mySongs.getSongs().get(0)));
	}

	@Test
	public void testAddMultipleSongs() {
		Song song1 = new Song("Title1", "Artist1", 1, 0, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 0, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 0, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertAll(()-> assertEquals(3, mySongs.size()),
				  ()-> assertEquals(song1, mySongs.getSongs().get(0)),
				  ()-> assertEquals(song2, mySongs.getSongs().get(1)),
				  ()-> assertEquals(song3, mySongs.getSongs().get(2)));
	}

}
