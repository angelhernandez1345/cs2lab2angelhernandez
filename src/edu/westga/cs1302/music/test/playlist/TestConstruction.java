package edu.westga.cs1302.music.test.playlist;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.resources.UI;

/**
 * Ensures correct functionality of the Playlist constructor(s).
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class TestConstruction {

	@Test
	public void testDefaultPlaylist() {
		Playlist playlist = new Playlist();
		assertAll(() -> assertEquals(0, playlist.size()), 
				  () -> assertEquals(Playlist.DEFAULT_NAME, playlist.getName()));
	}

	@Test
	public void testPlaylistNullName() {
		IllegalArgumentException iae = assertThrows(IllegalArgumentException.class, 
				     () -> new Playlist(null));
		assertEquals(UI.ExceptionMessages.PLAYLIST_NAME_NULL, iae.getMessage());
	}

	@Test
	public void testPlaylistEmptyName() {
		IllegalArgumentException iae = assertThrows(IllegalArgumentException.class, 
			     () -> new Playlist(""));
	assertEquals(UI.ExceptionMessages.PLAYLIST_NAME_EMPTY, iae.getMessage());
	}

	@Test
	public void testValidName() {
		Playlist playlist = new Playlist("Test");
		assertAll(() -> assertEquals(0, playlist.size()), 
				  () -> assertEquals("Test", playlist.getName()));
	}

}
