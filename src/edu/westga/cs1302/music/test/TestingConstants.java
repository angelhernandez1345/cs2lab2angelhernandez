package edu.westga.cs1302.music.test;

/**
 * The class TestingConstants.
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public final class TestingConstants {

	public static final double DELTA = 0.000001;

}
