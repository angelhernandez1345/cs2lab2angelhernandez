package edu.westga.cs1302.music.output;

import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.resources.UI;

/**
 * This class is responsible for generating reports.
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class ReportGenerator {

	/**
	 * Builds a full summary report of the specified playlist. If playlist is null,
	 * instead of throwing an exception it will return a string saying "No playlist
	 * exists.", otherwise builds a summary report of the playlist.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param playlist the playlist
	 *
	 * @return A formatted summary string of the playlist's songs.
	 */
	public String buildFullSummaryReport(Playlist playlist) {
		String summary = "";

		if (playlist == null) {
			summary = "No playlist exists.";
		} else {
			summary = playlist.getName() + System.lineSeparator();
			summary += "#Songs: " + playlist.size();
		}
		
		int duration = playlist.getDuration();
		summary += System.lineSeparator();
		summary += "Approximate duration: " + duration + " minutes" + System.lineSeparator();
		
		if (playlist.size() > 0) {
			Song shortestSong = playlist.getShortestSong();
			Song longestSong = playlist.getLongestSong();

			if (shortestSong != null && longestSong != null) {
				summary += "Shortest song:\t";
				summary += this.buildSongOutput(shortestSong) + System.lineSeparator();
				summary += "Longest song:\t";
				summary += this.buildSongOutput(longestSong) + System.lineSeparator();
			}
		}
		return summary;
	}

	private String buildSongOutput(Song song) {
		String output = song + UI.Text.COMMA_SPACE_SEPARATOR;
		output += String.format("%02d:%02d", song.getMinutes(), song.getSeconds());
		return output;
	}
}
