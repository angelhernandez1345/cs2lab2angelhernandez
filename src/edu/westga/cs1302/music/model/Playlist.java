package edu.westga.cs1302.music.model;

import java.util.ArrayList;

import edu.westga.cs1302.music.resources.UI;

/**
 * This class represents a playlist.
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class Playlist {

	public static final String DEFAULT_NAME = "Untitled";
	private String name;
	private ArrayList<Song> songs;

	/**
	 * Constructs a default playlist.
	 * 
	 * @precondition none
	 * @postcondition getName() == DEFAULT_NAME && size() == 0
	 */
	public Playlist() {
		this.name = DEFAULT_NAME;
		this.songs = new ArrayList<Song>();
	}

	/**
	 * Constructs a playlist with the specified name and no songs.
	 * 
	 * @precondition name != null && !name.isEmpty()
	 * @postcondition getName() == name && size() == 0
	 * @param name the specified name of this playlist
	 */
	public Playlist(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_NAME_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_NAME_EMPTY);
		}
		this.name = name;
		this.songs = new ArrayList<Song>();
	}

	/**
	 * Returns the name of this playlist.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name of this playlist.
	 * 
	 * @precondition name != null && !name.isEmpty().
	 * @postcondition getName() == name
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_NAME_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_NAME_EMPTY);
		}
		this.name = name;
	}

	/**
	 * Returns the songs of this playlist.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the songs
	 */
	public ArrayList<Song> getSongs() {
		return this.songs;
	}

	/**
	 * Adds the song to the playlist (allows duplicates)
	 * 
	 * @precondition song != null
	 * @postcondition size() == size()@prev + 1
	 *
	 * @param song the song to be added
	 * @return true if add successful
	 */
	public boolean add(Song song) {
		if (song == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SONG_NULL);
		}
		return this.songs.add(song);
	}

	/**
	 * Returns the size of the collection of songs (the number of songs).
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the size of the collection
	 */
	public int size() {
		return this.songs.size();
	}

	/**
	* Deletes the specified song from the playlist.
	*
	* @precondition none
	* @postcondition if found, size() == size()@prev – 1
	* @param song the song to delete from the playlist
	* @return true if the song was found and deleted, false otherwise
	*/
	public boolean removeSong(Song song) {
		if (this.checkIfSongExist(song)) {
			return this.songs.remove(song);
		}
		
		return false;
	}

	private boolean checkIfSongExist(Song song) {
		for (Song currSong : this.songs) {
			if (currSong.getTitle() == song.getTitle()) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Accepts an ArrayList of Song objects and adds them to the playlist.
	 * 
	 * @precondition songs != null
	 * @postcondition none
	 * @param songs ArrayList of songs
	 */
	public void addSongsToPlaylist(ArrayList<Song> songs) {
		if (songs != null) {
			for (Song song : songs) {
				this.songs.add(song);
			}
		}
	}
	
	/**
	 * Returns the duration of the playlist in minutes, rounded to the nearest
	 * minute. If the number of seconds is strictly less than 30, round to the
	 * previous minute. If the number of seconds is 30 or more, round to the next
	 * minute. For example, if the duration is 5 minutes and 29 seconds, return 5.
	 * If the duration is 10 minutes and 30 seconds, return 11 minutes.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the rounded duration in minutes
	 */
	public int getDuration() {
		int minutes = 0;
		int seconds = 0;
		for (Song currSong : this.songs) {
			minutes += currSong.getMinutes();
			seconds += currSong.getSeconds();
			if (seconds >= 60) {
				minutes++;
				seconds -= 60;
			}
		}
		if (seconds >= 30) {
			minutes++;
		}
		return minutes;
	}

	@Override
	public String toString() {
		return this.name + ": " + this.size() + " songs.";
	}

	/**
	 * Returns longest song from the playlist. If multiple songs have the same
	 * longest length, it will return the first encountered song.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return longest song (first in order) or null if no songs
	 */
	public Song getLongestSong() {
		// TODO VI.5.
		return null;
	}

	/**
	 * Returns shortest song from the playlist. If multiple songs have the same
	 * shortest length, it will return the first encountered song. If list is empty
	 * return null.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return shortest song (first in order) or null if no songs
	 */
	public Song getShortestSong() {
		// TODO VI.6.
		return null;
	}

}
