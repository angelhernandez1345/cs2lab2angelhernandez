package edu.westga.cs1302.music.model;

import edu.westga.cs1302.music.resources.UI;

/**
 * This class represents a song.
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class Song {

	public static final int SECONDS_UPPER_BOUND = 59;

	private String title;
	private String artist;
	private int minutes;
	private int seconds;
	private int year;

	/**
	 * Constructs a new song with specified attributes.
	 * 
	 * @precondition title != null && !title.isEmpty() && artist != null &&
	 *               !artist.isEmpty() && minutes >= 0 && seconds >= 0 && seconds <=
	 *               SECONDS_UPPER_BOUND && year > 0 && minutes and seconds cannot
	 *               be both zero at the same time
	 * @postcondition getTitle() == title && getArtist() == artist && getMinutes()
	 *                == minutes && getSeconds() == seconds && getYear() == year
	 * @param title   the title of this song
	 * @param artist  the artist of this song
	 * @param minutes the minutes of this song
	 * @param seconds the seconds of this song
	 * @param year    the year of this song
	 */
	public Song(String title, String artist, int minutes, int seconds, int year) {
		if (title == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.TITLE_NULL);
		}
		if (title.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.TITLE_EMPTY);
		}
		if (artist == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_NULL);
		}
		if (artist.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_EMPTY);
		}
		if (minutes == 0 && seconds == 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DURATION_ZER0);
		}
		if (minutes < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MINUTES_NEGATIVE);
		}
		if (seconds < 0 || seconds > SECONDS_UPPER_BOUND) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SECONDS_INVALID);
		}
		if (year <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.YEAR_INVALID);
		}

		this.title = title;
		this.artist = artist;
		this.minutes = minutes;
		this.seconds = seconds;
		this.year = year;
	}

	/**
	 * Returns the title of this song.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title of this song.
	 * 
	 * @precondition title != null && !title.isEmpty()
	 * @postcondition getTitle() == title
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		if (title == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.TITLE_NULL);
		}
		if (title.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.TITLE_EMPTY);
		}
		this.title = title;

	}

	/**
	 * Returns the artist of this song.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the artist
	 */
	public String getArtist() {
		return this.artist;
	}

	/**
	 * Sets the artist of this song.
	 * 
	 * @precondition artist != null && artist.isEmpty()
	 * @postcondition getArtist() == artist
	 * @param artist the artist to set
	 */
	public void setArtist(String artist) {
		if (artist == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_NULL);
		}
		if (artist.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_EMPTY);
		}
		this.artist = artist;
	}

	/**
	 * Returns the minutes of this song.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the minutes
	 */
	public int getMinutes() {
		return this.minutes;
	}

	/**
	 * Sets the minutes of this song.
	 * 
	 * @precondition minutes >= 0
	 * @postcondition getMinutes() == minutes iff (getMinutes() != 0 && getSeconds()
	 *                != 0)
	 * @param minutes the minutes to set
	 */
	public void setMinutes(int minutes) {
		if (minutes < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MINUTES_NEGATIVE);
		}
		if (this.seconds == 0 && minutes == 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DURATION_ZER0);
		}
		this.minutes = minutes;
	}

	/**
	 * Returns the seconds of this song.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the seconds
	 */
	public int getSeconds() {
		return this.seconds;
	}

	/**
	 * Sets the seconds of this song.
	 * 
	 * @precondition seconds >= 0 && seconds <= SECONDS_UPPER_BOUND
	 * @postcondition getSeconds() == seconds iff (getMinutes() != 0 && getSeconds()
	 *                != 0)
	 * @param seconds the seconds to set
	 */
	public void setSeconds(int seconds) {
		if (seconds < 0 || seconds > SECONDS_UPPER_BOUND) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SECONDS_INVALID);
		}
		if (this.minutes == 0 && seconds == 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DURATION_ZER0);
		}
		this.seconds = seconds;
	}

	/**
	 * Returns the year of this song.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the year
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * Sets the year of this song.
	 * 
	 * @precondition year > 0
	 * @postcondition getYear() == year
	 * @param year the year to set
	 */
	public void setYear(int year) {
		if (year <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.YEAR_INVALID);
		}
		this.year = year;
	}

	@Override
	public String toString() {
		return this.title + " - " + this.artist;
	}

}
